const wrapper = document.getElementById("signature-pad");
const clearButton = wrapper.querySelector("[data-action=clear]");
const saveJPGButton = wrapper.querySelector("[data-action=save-jpg]");
const canvas = wrapper.querySelector("canvas");

const signaturePad = new SignaturePad(canvas, {
  minWidth:2,
  maxWidth: 3,
  backgroundColor: 'rgb(255, 255, 255)'
});

const resizeCanvas = ()=>{
  // When zoomed out to less than 100%, for some very strange reason,
  // some browsers report devicePixelRatio as less than 1
  // and only part of the canvas is cleared then.
  const ratio =  Math.max(window.devicePixelRatio || 1, 1);

  // This part causes the canvas to be cleared
  canvas.width = canvas.offsetWidth * ratio;
  canvas.height = canvas.offsetHeight * ratio;
  canvas.getContext("2d").scale(ratio, ratio);

  signaturePad.clear();
}

// On mobile devices it might make more sense to listen to orientation change,
// rather than window resize events.
window.onresize = resizeCanvas;
resizeCanvas();

const dataURLToBlob = (dataURL)=>{
    
  const parts = dataURL.split(';base64,');
  const contentType = parts[0].split(":")[1];
  const raw = window.atob(parts[1]);
  const rawLength = raw.length;
  const uInt8Array = new Uint8Array(rawLength);

  for (const i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }

  return new Blob([uInt8Array], { type: contentType });
}

clearButton.addEventListener("click", function (event) {
  signaturePad.clear();
});

const download = (dataURL, filename)=>{
  if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
    window.open(dataURL);
  } else {
    const blob = dataURLToBlob(dataURL);
    const url = window.URL.createObjectURL(blob);

    const a = document.createElement("a");
    a.style = "display: none";
    a.href = url;
    a.download = filename;

    document.body.appendChild(a);
    a.click();

    window.URL.revokeObjectURL(url);
  }
}

saveJPGButton.addEventListener("click", function (event) {
  event.preventDefault();
  if (signaturePad.isEmpty()) {
    alert("Please provide a signature first.");
  } else {
      
    const dataURL = signaturePad.toDataURL("../images/jpeg");

    //convert image to pdf
    const pdf = new jsPDF();
    pdf.addImage(dataURL, 'JPEG', 55,50,50,50);
    pdf.save("download.pdf");
  
  }

});